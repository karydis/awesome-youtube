# Awesome Youtube

A list of channels I am currently subscribed to, that I believe are awesome!

## Science
[minutephysics](https://www.youtube.com/user/minutephysics) (Henry Reich)

[Veritasium](https://www.youtube.com/user/1veritasium) (Derek Muller)

[The Slo-Mo Guys](The Slow Mo Guys) (Gavin Free, Daniel Gruchy)

[Vsauce](https://www.youtube.com/user/Vsauce) (Michael Stevens)
  - [Braincandy Live](https://www.ticketmaster.com/Brain-Candy-Live-tickets/artist/2281549)
  - [Curiosity Box](https://www.curiositybox.com/)
  - [Mindfield](https://www.youtube.com/playlist?list=PLZRRxQcaEjA7wmh3Z6EQuOK9fm1CqnJCI)
  - [DONG](https://www.youtube.com/channel/UClq42foiSgl7sSpLupnugGA)
  - [Vsauce3](https://www.youtube.com/user/Vsauce3) (Jake Roper)
  

## Maths
[3Blue1Brown](https://www.youtube.com/channel/UCYO_jab_esuFRV4b17AJtAw) (Grand Sanderson)

[StandupMaths](https://www.youtube.com/user/standupmaths) (Matt Parker)

[SteveMould](https://www.youtube.com/user/steventhebrave)

[Numberphile](https://www.youtube.com/user/numberphile) (James Grime)

## Engineering
[SmarterEveryDay](https://www.youtube.com/user/destinws2) (Destin Sandlin)

[Practical Engineering](https://www.youtube.com/user/gradyhillhouse)

[What's Inside](https://www.youtube.com/user/lincolnmarkham) (Lincoln Markham)

## Computer Science
[Computerphile](https://www.youtube.com/user/Computerphile)

## Music Theory
[Adam Neely](https://www.youtube.com/user/havic5)
